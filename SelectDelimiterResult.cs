﻿namespace LoadCSVWinForm
{
    /// <summary>
    /// Class for transfer of data between forms
    /// </summary>
    public class SelectDelimiterResult
    {
        public SelectDelimiterResult()
        {
        }

        /// <summary>
        /// Constructor for SelectDelimiterResult initialized with delimiter
        /// </summary>
        /// <param name="delimiter">CSV delimiter</param>
        public SelectDelimiterResult(char delimiter)
        {
            Delimiter = delimiter;
        }

        public char Delimiter { get; set; }
    }
}
