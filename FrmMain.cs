﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;

namespace LoadCSVWinForm
{
    public partial class FrmMain : Form
    {
        private readonly FileReader _fileReader;
        private readonly TextHelper _textHelper;
        private readonly GradientHelper _gradientHelper;
        private List<string> _headers;
        private int _titleColumnIndex = -1;
        private int _authorColumnIndex = -1;
        private int _yearColumnIndex = -1;
        private int _priceColumnIndex = -1;
        private int _inStockColumnIndex = -1;
        private int _bindingColumnIndex = -1;
        private int _descriptionColumnIndex = -1;
        private Dictionary<double, Color> _dicPriceColor;
        public FrmMain()
        {
            InitializeComponent();
            _fileReader = new FileReader();
            _textHelper = new TextHelper();
            _gradientHelper = new GradientHelper();
        }

        private void OpenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                dgvData.DataSource = null;
                dgvData.Rows.Clear();
                dgvData.Columns.Clear();
                OpenFileDialog dialog = new OpenFileDialog
                {
                    Title = StaticVariables.openFiledialogTitle,
                    Filter = StaticVariables.openFiledialogFilter,
                    RestoreDirectory = true
                };
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    string fileText = System.IO.File.ReadAllText(dialog.FileName);
                    SelectDelimiterResult selectDelimiterResult = InitializeSelectDelimeterResult(fileText);
                    FrmSelectDelimiter frmSelectDelimiter = new FrmSelectDelimiter(selectDelimiterResult);
                    if (DialogResult.OK == frmSelectDelimiter.ShowDialog())
                    {
                        ReadCSV(dialog.FileName, selectDelimiterResult.Delimiter);
                    }
                }
                else
                {
                    return;
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        /// <summary>
        /// Find the most occured delimiter by file text
        /// </summary>
        /// <param name="fileText">File text</param>
        /// <returns>
        /// Delimiter result
        /// </returns>
        private SelectDelimiterResult InitializeSelectDelimeterResult(string fileText)
        {
            SelectDelimiterResult selectDelimiterResult = new SelectDelimiterResult();
            try
            {

                char charWithMaxOccurrence = _textHelper.GetCharWithMaxOccurence(fileText, StaticVariables.knownDelimiters);
                selectDelimiterResult = new SelectDelimiterResult(charWithMaxOccurrence);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
            return selectDelimiterResult;
        }


        /// <summary>
        /// Close the app
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">e</param>
        private void CloseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Read the csv file
        /// </summary>
        /// <param name="path">File path</param>
        /// <param name="delimiter">Delimiter</param>
        public void ReadCSV(string path, char delimiter)
        {
            string[] CSVLines = _fileReader.ReadFileLines(path);
            if (CSVLines.Length > 0)
            {
                _headers = CSVLines[StaticVariables.csvheaderRowIndex].Split(delimiter).ToList<string>();
                AddDataGridViewColumns(_headers);
                _titleColumnIndex = _headers.IndexOf(StaticVariables.titleColumn);
                _authorColumnIndex = _headers.IndexOf(StaticVariables.authorColumn);
                _yearColumnIndex = _headers.IndexOf(StaticVariables.yearColumn);
                _priceColumnIndex = _headers.IndexOf(StaticVariables.priceColumn);
                _inStockColumnIndex = _headers.IndexOf(StaticVariables.inStockColumn);
                _bindingColumnIndex = _headers.IndexOf(StaticVariables.binidngColumn);
                _descriptionColumnIndex = _headers.IndexOf(StaticVariables.descriptionColumn);
                List<string> lstPossibleBindings = GetPossibleBindings(delimiter, CSVLines, _bindingColumnIndex);
                AddBindingItemsToColumn(_bindingColumnIndex, lstPossibleBindings);
                for (int rowIndex = StaticVariables.csvheaderRowIndex + 1; rowIndex < CSVLines.Length; rowIndex++)
                {
                    List<string> rowValues = CSVLines[rowIndex].Split(delimiter).ToList<string>();
                    string title = rowValues[_titleColumnIndex];
                    string author = rowValues[_authorColumnIndex];
                    string year = rowValues[_yearColumnIndex];
                    string price = rowValues[_priceColumnIndex];
                    string inStock = rowValues[_inStockColumnIndex];
                    bool isInStock = (inStock == StaticVariables.inStockColumn_TrueValue);
                    string binding = rowValues[_bindingColumnIndex];
                    string description = rowValues[_descriptionColumnIndex];
                    dgvData.Rows.Add();
                    int lastRowIndex = dgvData.Rows.Count - 1;
                    dgvData[_titleColumnIndex, lastRowIndex].Value = title;
                    dgvData[_authorColumnIndex, lastRowIndex].Value = author;
                    dgvData[_yearColumnIndex, lastRowIndex].Value = year;
                    if (double.TryParse(price.Replace(",", "."), NumberStyles.Any, CultureInfo.GetCultureInfo("en-US"), out double dblPrice))
                    {
                        dgvData[_priceColumnIndex, lastRowIndex].Value = dblPrice;
                    }
                    dgvData[_inStockColumnIndex, lastRowIndex].Value = isInStock;
                    dgvData[_bindingColumnIndex, lastRowIndex].Value = binding;
                    dgvData[_descriptionColumnIndex, lastRowIndex].Value = "Click me!";
                    dgvData[_descriptionColumnIndex, lastRowIndex].ToolTipText = description;
                }
                dgvData.Sort(dgvData.Columns[_priceColumnIndex], System.ComponentModel.ListSortDirection.Ascending);
                CalculateGradient();
                CalculateHighlight();
            }
        }

        /// <summary>
        /// Calculates if row should be highlighted
        /// </summary>
        private void CalculateHighlight()
        {
            for (int rowIndex = 0; rowIndex < dgvData.Rows.Count; rowIndex++)
            {
                bool inStock = (bool)dgvData[_inStockColumnIndex, rowIndex].Value;
                if (!inStock)
                {
                    dgvData.Rows[rowIndex].DefaultCellStyle.BackColor = Color.Yellow;
                    dgvData[_descriptionColumnIndex, rowIndex].Style.BackColor = Color.Yellow;
                }
                else
                {
                    dgvData.Rows[rowIndex].DefaultCellStyle.BackColor = Color.White;
                    dgvData[_descriptionColumnIndex, rowIndex].Style.BackColor = Color.White;
                }
            }
        }

        /// <summary>
        /// Calculates the gradient text color of prices
        /// </summary>
        private void CalculateGradient()
        {
            List<double> lstPossiblePrices = GetPossiblePrices();
            _dicPriceColor = _gradientHelper.GetGradient(Color.Black, Color.LightGray, lstPossiblePrices);
            for (int rowIndex = 0; rowIndex < dgvData.Rows.Count; rowIndex++)
            {
                double dblPrice = (double)dgvData[_priceColumnIndex, rowIndex].Value;
                dgvData[_priceColumnIndex, rowIndex].Style.ForeColor = _dicPriceColor[dblPrice];
            }
        }

        /// <summary>
        /// Get possible prices
        /// </summary>
        /// <returns>
        /// List of prices
        /// </returns>
        private List<double> GetPossiblePrices()
        {
            List<double> lstPossiblePrices = new List<double>();
            for (int rowIndex = 0; rowIndex < dgvData.Rows.Count; rowIndex++)
            {
                double dblPrice = (double)dgvData[_priceColumnIndex, rowIndex].Value;
                if (!lstPossiblePrices.Contains(dblPrice))
                {
                    lstPossiblePrices.Add(dblPrice);
                }
            }
            lstPossiblePrices.Sort();
            return lstPossiblePrices;
        }

        /// <summary>
        /// Add biniding items to the comboboc column
        /// </summary>
        /// <param name="bindingColumnIndex"Binding column index</param>
        /// <param name="lstPossibleBindings">List of possible bindings</param>
        private void AddBindingItemsToColumn(int bindingColumnIndex, List<string> lstPossibleBindings)
        {
            DataGridViewComboBoxColumn dataGridViewComboBoxColumn = (DataGridViewComboBoxColumn)this.dgvData.Columns[bindingColumnIndex];
            foreach (var item in lstPossibleBindings)
            {
                dataGridViewComboBoxColumn.Items.Add(item);
            }
        }

        /// <summary>
        /// Get possible bindings
        /// </summary>
        /// <param name="delimiter">Csv file delimiter</param>
        /// <param name="CSVLines">Csv lines</param>
        /// <param name="bindingColumnIndex">Binding column index</param>
        /// <returns>
        /// List of possible bindings
        /// </returns>
        private List<string> GetPossibleBindings(char delimiter, string[] CSVLines, int bindingColumnIndex)
        {
            List<string> lstPossibleBindings = new List<string>();
            for (int rowIndex = StaticVariables.csvheaderRowIndex + 1; rowIndex < CSVLines.Length; rowIndex++)
            {
                List<string> rowValues = CSVLines[rowIndex].Split(delimiter).ToList<string>();
                string binding = rowValues[bindingColumnIndex];
                if (!lstPossibleBindings.Contains(binding))
                {
                    lstPossibleBindings.Add(binding);
                }
            }
            return lstPossibleBindings;
        }

        /// <summary>
        /// Create datagridview columns
        /// </summary>
        /// <param name="headers">List of hear texts</param>
        private void AddDataGridViewColumns(List<string> headers)
        {
            foreach (string header in headers)
            {
                if (header == StaticVariables.titleColumn ||
                    header == StaticVariables.authorColumn ||
                    header == StaticVariables.yearColumn ||
                    header == StaticVariables.priceColumn)
                {
                    dgvData.Columns.Add(header, header);
                }
                else if (header == StaticVariables.inStockColumn)
                {
                    DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn = new DataGridViewCheckBoxColumn
                    {
                        HeaderText = header,
                        Name = header
                    };
                    dgvData.Columns.Add(dataGridViewCheckBoxColumn);
                }
                else if (header == StaticVariables.binidngColumn)
                {
                    DataGridViewComboBoxColumn dataGridViewComboBoxColumn = new DataGridViewComboBoxColumn
                    {
                        HeaderText = header,
                        Name = header
                    };
                    dataGridViewComboBoxColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
                    dgvData.Columns.Add(dataGridViewComboBoxColumn);
                }
                else if (header == StaticVariables.descriptionColumn)
                {
                    DataGridViewButtonColumn dataGridViewButtonColumn = new DataGridViewButtonColumn
                    {
                        HeaderText = header,
                        Name = header,
                    };
                    dataGridViewButtonColumn.FlatStyle = FlatStyle.Popup;
                    dgvData.Columns.Add(dataGridViewButtonColumn);
                }
            }
        }

        /// <summary>
        /// Show message box in case user clicks on button column buttons
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">e</param>
        private void DgvData_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
            {
                MessageBox.Show(dgvData[e.ColumnIndex, e.RowIndex].ToolTipText, "Info", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else if (senderGrid.Columns[e.ColumnIndex] is DataGridViewCheckBoxColumn && e.RowIndex >= 0)
            {
                CalculateHighlight();
            }
        }

        /// <summary>
        /// Let the drop down opens with one click
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">e</param>
        private void DgvData_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            bool validClick = (e.RowIndex != -1 && e.ColumnIndex != -1);
            var datagridview = sender as DataGridView;
            if (datagridview.Columns[e.ColumnIndex] is DataGridViewComboBoxColumn && validClick)
            {
                datagridview.BeginEdit(true);
                ((ComboBox)datagridview.EditingControl).DroppedDown = true;
            }
        }

        /// <summary>
        /// Let the drop down opens with one click
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">e</param>
        private void DgvData_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            dgvData.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }

        /// <summary>
        /// Delete rows which does not have stocks
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">E</param>
        private void TsbDeleteNotInStockRows_Click(object sender, EventArgs e)
        {
            List<DataGridViewRow> lstDataGridViewrowtoBeDeleted = new List<DataGridViewRow>();
            for (int currentRowIndex = 0; currentRowIndex < dgvData.Rows.Count; currentRowIndex++)
            {
                bool inStock = (bool)dgvData[_inStockColumnIndex, currentRowIndex].Value;
                if (!inStock)
                {
                    lstDataGridViewrowtoBeDeleted.Add(dgvData.Rows[currentRowIndex]);
                }
            }
            lstDataGridViewrowtoBeDeleted.ForEach(d => dgvData.Rows.Remove(d));
            CalculateGradient();
            CalculateHighlight();
        }
    }
}
