﻿using System.Collections.Generic;
using System.Drawing;

namespace LoadCSVWinForm
{
    public class GradientHelper
    {
        public Dictionary<double, Color> GetGradient(Color highColor, Color lowColor,List<double> lstValues)
        {
            Dictionary<double, Color> keyValuePairs = new Dictionary<double, Color>();
            int rMax = highColor.R;
            int rMin = lowColor.R;
            int gMax = highColor.G;
            int gMin = lowColor.G;
            int bMax = highColor.B;
            int bMin = lowColor.B;
            int size = lstValues.Count;
            for (int currentValueIndex = 0; currentValueIndex < size; currentValueIndex++)
            {
                var rAverage = rMin + (int)((double)(rMax - rMin) * (double)currentValueIndex / (double)size);
                var gAverage = gMin + (int)((double)(gMax - gMin) * (double)currentValueIndex / (double)size);
                var bAverage = bMin + (int)((double)(bMax - bMin) * (double)currentValueIndex / (double)size);
                keyValuePairs.Add(lstValues[currentValueIndex], Color.FromArgb(rAverage, gAverage, bAverage));
            }
            return keyValuePairs;
        }
    }
}
