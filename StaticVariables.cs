﻿using System.Collections.Generic;

namespace LoadCSVWinForm
{
    public class StaticVariables
    {
        public static List<char> knownDelimiters = new List<char>() { ',', ';', '|' };
        public static string titleColumn = "Title";
        public static string authorColumn = "Author";
        public static string yearColumn = "Year";
        public static string priceColumn = "Price";
        public static string inStockColumn = "In Stock";
        public static string descriptionColumn = "Description";
        public static string binidngColumn = "Binding";
        public static string inStockColumn_TrueValue = "yes";
        public static string inStockColumn_FalseValue = "No";
        public static int csvheaderRowIndex = 0;
        public static string openFiledialogTitle = "Open a CSV File";
        public static string openFiledialogFilter = "CSV Files (*.csv)|*.csv";

    }
}
