﻿using System;
using System.Windows.Forms;

namespace LoadCSVWinForm
{
    public partial class FrmSelectDelimiter : Form
    {
        private readonly SelectDelimiterResult _selectDelimiterResult;
        public FrmSelectDelimiter(SelectDelimiterResult selectDelimiterResult)
        {
            InitializeComponent();
            _selectDelimiterResult = selectDelimiterResult;
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void BtnOK_Click(object sender, EventArgs e)
        {
            if(char.TryParse(cmbDelimiter.SelectedItem.ToString(), out char selectedDelimiter))
            {
                _selectDelimiterResult.Delimiter = selectedDelimiter;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void FrmSelectDelimiter_Load(object sender, EventArgs e)
        {
            foreach (var character in StaticVariables.knownDelimiters)
            {
                cmbDelimiter.Items.Add(character);
            }
            if (StaticVariables.knownDelimiters.Contains(_selectDelimiterResult.Delimiter))
            {
                cmbDelimiter.SelectedIndex = StaticVariables.knownDelimiters.IndexOf(_selectDelimiterResult.Delimiter);
            }
            else
            {
                cmbDelimiter.SelectedIndex = 0;
            }
        }
    }
}
