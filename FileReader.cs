﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LoadCSVWinForm
{
    public class FileReader
    {
        /// <summary>
        /// Get file line given file path
        /// </summary>
        /// <param name="path">file path</param>
        /// <returns>Array containing file lines</returns>
        public string[] ReadFileLines(string path)
        {
            List<string> lstLines = new List<string>();
            using (System.IO.FileStream fileStream = new System.IO.FileStream(path, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read))
            {
                using (System.IO.StreamReader sr = new System.IO.StreamReader(fileStream, Encoding.UTF8))
                {
                    string line = String.Empty;
                    while ((line = sr.ReadLine()) != null)
                    {
                        lstLines.Add(line);
                    }
                }
            }
            return lstLines.ToArray();
        }
    }
}
