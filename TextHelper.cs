﻿using System.Collections.Generic;
using System.Linq;

namespace LoadCSVWinForm
{
    public class TextHelper
    {
        /// <summary>
        /// Gets the char with max occurence in a text
        /// </summary>
        /// <param name="fileText">File text</param>
        /// <param name="chars">chars</param>
        /// <returns>
        /// The char with max occurence
        /// </returns>
        public char GetCharWithMaxOccurence(string fileText, List<char> chars)
        {
            Dictionary<char, int> dicCharCount = new Dictionary<char, int>();
            foreach (var character in chars)
            {
                int count = fileText.Count(f => f == character);
                dicCharCount.Add(character, count);
            }
            var charWithMaxOccurrence = dicCharCount.FirstOrDefault(x => x.Value == dicCharCount.Values.Max()).Key;
            return charWithMaxOccurrence;
        }
    }
}
